
var clientesObtenidos;
var urlbase = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";

function getClientes(){
  //var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers?$filter=Country eq 'Germany'";
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";

  var request = new XMLHttpRequest();
  request.onreadystatechange = function(){
    if(this.readyState == 4 && this.status == 200){
      console.log(request.responseText);
      clientesObtenidos = request.responseText;
      procesarClientes();
    }
  }

  request.open("GET", url, true);
  request.send();
}


function procesarClientes(){
  var JSONClientes = JSON.parse(clientesObtenidos);
  //alert(JSONProductos.value[0].ProductName);
  var divTabla = document.getElementById('divTablaCli');
  var tabla = document.createElement('table');
  var tbody = document.createElement('tbody');
  tabla.classList.add('table');
  tabla.classList.add('table-striped');


  for (var i = 0; i < JSONClientes.value.length; i++) {
    //console.log(JSONProductos.value[i].ProductName);
    var nuevaFila = document.createElement('tr');
    var columnaId = document.createElement('td');
    columnaId.innerText = JSONClientes.value[i].CompanyName;

    var columnaNombre = document.createElement('td');
    columnaNombre.innerText = JSONClientes.value[i].City;

    var columnaCountry = document.createElement('td');
    var imgCountry = document.createElement('img');
    if(JSONClientes.value[i].Country =='UK'){
      imgCountry.src = urlbase + 'United-Kingdom' + ".png";
    }else{
      imgCountry.src = urlbase + JSONClientes.value[i].Country + ".png";
    }
    //imgCountry.width = "30";
    imgCountry.classList.add('flag');
    columnaCountry.appendChild(imgCountry);

    nuevaFila.appendChild(columnaId);
    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaCountry);

    tbody.appendChild(nuevaFila);
  }

  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);

}
